This python script is able to plot spherical histograms, you can see an example here.
You just need mayavi and numpy to run the script.


![snapshot](./snap.png "snapshot").

MIT License, Copyright (c) 2024, Damien Andre, Limoges University
